"""
    Key-Phrase Substitution Cipher
    Author: Ryan Spurr
"""

# simple alphabet
alphabet = "abcdefghijklmnopqrstuvwxyz"

if __name__ == "__main__":

    '''===================== SETUP ====================='''

    # eliminate duplicates and uppercase values
    key = raw_input("[+] Enter keyphrase: ")

    # keyphrase cant be longer than the alphabet
    assert len(key) <= len(alphabet)

    key_list = []
    for c in key:
        if c not in key_list and c.isalpha():
            key_list.append(c.lower())

    # create an alphabet dictionary
    # without characters from our keyphrase
    alpha_list = []
    for c in alphabet:
        if c in key_list:
            continue
        else:
            alpha_list.append(c)

    '''===================== CREATE CIPHER ====================='''

    # Create an encryption cipher as a list, beginning with our
    # keyphrase and ending with the rest of our alphabet
    enc_list = []
    for i in range(0, len(alphabet)):
        try:
            enc_list.append(key_list[i])
        except IndexError:
            break
    enc_list += alpha_list

    # Create mapping between original alphabet
    # and the encrypted alphabet
    enc_mapping = {}
    alpha_ctr = 0
    for c in enc_list:
        enc_mapping[alphabet[alpha_ctr]] = c
        alpha_ctr += 1

    '''===================== ENCRYPT PLAINTEXT ====================='''

    # get plain_text
    with open("prose.txt", 'r') as f:
        plain_text = f.read().replace('\n', '')

    plain_text = plain_text.lower()

    print "[+] Plaintext: {}".format(plain_text)

    # encrypt the plain_text by substituting characters
    cipher_text = []
    for c in plain_text:
        if c in enc_mapping.keys():
            cipher_text.append(enc_mapping[c])
    cipher_text = "".join(cipher_text)

    # create a list of slice groups of 5 from cipher_text
    ct_grouped = [cipher_text[i:i+5] for i in range(0, len(cipher_text), 5)]
    # join delimited by a space
    cipher_text = " ".join(ct_grouped)

    print "[+] Ciphertext: {}".format(cipher_text)

    '''===================== DECRYPT CIPHERTEXT ====================='''

    # flip encryption mappings to decrypt ciphertext
    dec_mapping = {v : k for k, v in enc_mapping.iteritems()}

    # decrypt what we can and join+print as a string
    plain_text = []
    for c in "".join(cipher_text):
        try:
            plain_text.append(dec_mapping[c])
        except KeyError:
            continue

    print "[+] Decrypted text: {}".format("".join(plain_text))


